import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Handles system input stream reading.
 */
public abstract class AbstractSystemInputReader {
    /**
     * Reads system input stream line by line. All characters are converted to lower case and each
     * line is passed for processing to {@link #processLine(String)} method.
     * 
     * @throws IOException if an I/O error occurs
     */
    public void readSystemInput() throws IOException {
        StringBuilder line = new StringBuilder();
        int c;
        while ((c = System.in.read()) >= 0) {
            if (c == '\r' || c == '\n') {
                processLine(line.toString().toLowerCase().trim());
                line.setLength(0);
            } else {
                line = line.append((char)c);
            }
        }
    }
    public void readBufferedInput() throws IOException
    {
    	//System.out.println("hi");
    	ParserBot p=new ParserBot(System.in);
    	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    	String c;
    	while((c=br.readLine())!=null)
    	{
    	//String temp=p.nextString();
    	//if(temp.equals("end"))
    		//break;
    	processLine(c);
    	}
    }
    
    /**
     * Process a line read out by {@link #readSystemInput()} method in a way defined by subclass
     * implementation.
     * 
     * @param line single, trimmed line of system input
     */
    public abstract void processLine(String line);
}
