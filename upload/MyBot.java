import java.io.IOException;
import java.util.*;

/**
 * Starter bot implementation.
 */
public class MyBot extends Bot {
	/**
	 * Main method executed by the game engine for starting the bot.
	 * 
	 * @param args command line arguments
	 * 
	 * @throws IOException if an I/O error occurs
	 */
	static Ants ants;

	static boolean debug=false;
	public static void main(String[] args) throws Exception {
		if(debug)
		{
			Tests.runTests();
			return;
		}
		new MyBot().readBufferedInput();
		if(debug)
			Utils.log();//this has to be removed
	}

	//private Set<Tile> orders=new HashSet<Tile>();
	private Map<Tile, Tile> orders = new HashMap<Tile, Tile>();
	int [][]stepMatrix;
	

	private boolean doMoveDirection(Tile antLoc, Direction direction) {
		// Track all moves, prevent collisions
		Tile newLoc = ants.getTile(antLoc, direction);
		if (ants.getTileType(newLoc).isUnoccupied() && !orders.containsKey(newLoc)) {
			stepMatrix[newLoc.getRow()][newLoc.getCol()]=stepMatrix[newLoc.getRow()][newLoc.getCol()]+1;
			ants.issueOrder(antLoc, direction);
			orders.put(newLoc, antLoc);
			return true;
		} else {
			return false;
		}
	}
	private void doMoveDirectionSet(Map<Tile,Tile> moves)
	{
		for(Tile dest:moves.keySet())
		{
			Tile src=moves.get(dest);
			Direction d=Utils.whichDirection(src, dest, ants);
			ants.issueOrder(src, d);
			orders.put(dest, src);
		}
	}
	private boolean doMoveDirectionEnemyAware(Tile antLoc,Direction direction)
	{
		Tile newLoc = ants.getTile(antLoc, direction);
		Set<Tile> enemyAnts=ants.getEnemyAnts();
		for(Tile offset:ants.getAttackOffsets())
		{
			if(enemyAnts.contains(ants.getTile(newLoc, offset)))
				return false;
		}
		return doMoveDirection(antLoc,direction);
	}
	static int turn;
	@Override
	public void doTurn(){

		turn++;
		ants = getAnts();
		orders.clear();
		PathFind.ants=ants;
		run();//only one strategy available
		//System.out.println(turn);
	}

	public void run()
	{
		//prevent stepping on own hill
		if(turn>5)
		preventHillStepping();
		//gather food
		
		Set<Tile> availableAnts=new HashSet<Tile>();
		Set<Tile> availableFoods=new HashSet<Tile>();
		for(Tile ant:ants.getMyAnts())
			availableAnts.add(new Tile(ant.getRow(),ant.getCol()));
		for(Tile food:ants.getFoodTiles())
			availableFoods.add(new Tile(food.getRow(),food.getCol()));

		initExploreMatrix(-1);
		PathFind.bfsDistancesFromHill(ants.getMap(), ants.getMyInitHills(),exploreMatrix,ants.getExploredMap());//Init hills as of now
		gatherFood_ShortestDistanceFirst(availableAnts,availableFoods,true);
		
		if(ants.getMyInitHills().size()>2)
		{
			combat2(availableAnts,exploreMatrix);
		}
		else
		{
			if(turn>25)
				combat2(availableAnts,exploreMatrix);
		}
		//food gathering methods
			//gatherFood(availableAnts);
			//gatherFood_AntsToFood(availableAnts);

		//exploration
			//explore_BFS_unseen(availableAnts);
			//getPotentialRegions(availableAnts);
			//reachPotentialRegions(availableAnts);
			
			boolean suckingCells[][]=new boolean[ants.getRows()][ants.getCols()];
			findSuckingZones(suckingCells,exploreMatrix);
			
			
			PathFind.bfsDistancesFromEnemyHill(ants.getMap(), ants.getEnemyHills(),enemyHillMatrix,ants.getExploredMap());//Init hills as of now
			exploreTowardsEnemyHills(availableAnts,enemyHillMatrix);
			int r=ants.getRows();int c=ants.getCols();
			int val=(r+c)/6;
			for(int i=0;i<r;i++)
				for(int j=0;j<c;j++)
				{
					if(exploreMatrix[i][j]>val)
						exploreMatrix[i][j]=val;
				}

			exploreOutwards(availableAnts,exploreMatrix,suckingCells,val);
				//exploreLeastNext(availableAnts);

			
		// unblock hills
		unblockHills();
	}
	private void exploreTowardsEnemyHills(Set<Tile> availableAnts,int [][]enemyHillMatrix)
	{
		Set<Tile> toRemove=new HashSet<Tile>();
		for(Tile ant:availableAnts)
		{
			if(enemyHillMatrix[ant.getRow()][ant.getCol()]<15)
			{
				int cur=enemyHillMatrix[ant.getRow()][ant.getCol()];
				for(Direction d:Direction.values())
				{
					Tile newTile=ants.getTile(ant, d);
					if(enemyHillMatrix[newTile.getRow()][newTile.getCol()]<cur)
					{
						if(doMoveDirectionEnemyAware(ant,d)==true)
						{
							toRemove.add(ant);
							break;
						}
					}

				}

			}
		}
		availableAnts.removeAll(toRemove);
	}
	private void exploreLeastNext(Set<Tile> availableAnts)
	{
		for(Tile ant:availableAnts)
		{
			int min=Integer.MAX_VALUE;
			Direction dir=null;
			Set<Direction> allMoves=new HashSet<Direction>(shuffledDirs);
			for(int i=0;i<4;i++)
			{
				for(Direction d:allMoves)
				{
					Tile m=ants.getTile(ant, d);
					if(stepMatrix[m.getRow()][m.getCol()]<min)
					{
						min=stepMatrix[m.getRow()][m.getCol()];
						dir=d;
					}
				}
				if(doMoveDirectionEnemyAware(ant,dir)==true)
				{
					break;
				}
				allMoves.remove(dir);
				min=Integer.MAX_VALUE;dir=null;
			}
		}
	}
	private void combat2(Set<Tile> availableAnts,int [][]exploreMatrix)
	{
		Set<Tile> enemyAnts=ants.getEnemyAnts();
		TreeSet<Tile> sortedEnemyAnts=new TreeSet<Tile>(enemyAnts);
		boolean exploredMap[][]=ants.getExploredMap();
		TileType map[][]=ants.getMap();
		
		int r=ants.getRows();int c=ants.getCols();
		int combatMatrix[][]=new int[r][c];

		
		//create enemy groups
		Set<Tile> enemyProcessed=new HashSet<Tile>();
		HashMap<Double,HashSet<Tile>> listOfEnemyGroups=new HashMap<Double,HashSet<Tile>>();
		for(Tile enemy:sortedEnemyAnts)
		{
			if(enemyProcessed.contains(enemy))
				continue;


			HashSet<Tile> enemyGroup=new HashSet<Tile>();
			enemyGroup.add(enemy);
			enemyProcessed.add(enemy);
			
			LinkedList<Tile> aqueue=new LinkedList<Tile>();
			aqueue.add(enemy);
			
			
			while(aqueue.size()!=0)
			{
				Tile nenemy=aqueue.remove();
				for(Tile offset:ants.getAttackOffsets())
				{
					Tile enemy2=ants.getTile(nenemy, offset);
					if(enemyAnts.contains(enemy2) && !enemyGroup.contains(enemy2))
					{
						aqueue.add(enemy2);
						enemyGroup.add(enemy2);
						enemyProcessed.add(enemy2);
					}
				}
			}
			
			//find min distance enemy
			int minDist=Integer.MAX_VALUE;
			for(Tile e1:enemyGroup)
			{
				int x=exploreMatrix[e1.getRow()][e1.getCol()];
				if(x!=-1 && x<minDist)
					minDist=x;
			}
			if(minDist==Integer.MAX_VALUE)
				continue;
			if(listOfEnemyGroups.containsKey(minDist)){
				if(listOfEnemyGroups.containsKey(minDist+0.1)){
					listOfEnemyGroups.put(minDist+0.2, enemyGroup);
				}
				else{
					listOfEnemyGroups.put(minDist+0.1,enemyGroup);
				}
			}
			else{
				listOfEnemyGroups.put(minDist+0.0, enemyGroup);
			}
		}
		ArrayList<Double> al=new ArrayList<Double>(listOfEnemyGroups.keySet());
		Collections.sort(al);
		
		for(Double doubleDist:al)
		{
			HashSet<Tile> enemyGroup=listOfEnemyGroups.get(doubleDist);
			//HashSet<Tile> enemyGroup=listOfEnemyGroups.get(minDist);

			for(int i=0;i<ants.getRows();i++)
				Arrays.fill(combatMatrix[i], -1);
			int spread;
			if(turn<50)
				spread=(-1*(int)Math.log(doubleDist))+4;
			else if(turn>50 && turn<100)
				spread=(-1*(int)Math.log(doubleDist))+5;
			else
				spread=(-1*(int)Math.log(doubleDist))+8;
			if(doubleDist<25)
				spread=12;

			
			//Idea: For this group, create combat distances, select ants and do combat march/attack phase
			//For the next group remaining ants used. Groups has to be prioritized later
			//BFS here should consider water tiles also
			LinkedList<Tile> queue=new LinkedList<Tile>();
			int visited[][]=new int[r][c];
			for(Tile e:enemyGroup)
			{
				for(Tile offset:ants.getAttackRadiusOffsets())
				{
					Tile n=ants.getTile(e, offset);
					if(map[n.getRow()][n.getCol()]!=TileType.WATER)
					{
					combatMatrix[n.getRow()][n.getCol()]=0;
					queue.add(n);
					visited[n.getRow()][n.getCol()]=1;
					}
				}
			}
			Set<Tile> army=new HashSet<Tile>();
			
			while(queue.size()!=0)
			{
				Tile cur=queue.remove();
				int newr,newc;
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==1 || map[newr][newc]==TileType.WATER)
						continue;
					else
					{
						visited[newr][newc]=1;
						combatMatrix[newr][newc]=combatMatrix[cur.getRow()][cur.getCol()]+1;
						if(combatMatrix[newr][newc]<spread)//control of scent propagation
							queue.add(newNode);
						if(army.size()<(2*enemyGroup.size()) && availableAnts.contains(newNode))
						{
							army.add(newNode);
							availableAnts.remove(newNode);
						}

					}
					
				}
			}
			
			if(enemyGroup.size()==1 && doubleDist>18 && army.size()<2)
			{
				for(Tile ant:army)
					availableAnts.add(ant);
				continue;
			}
			
			//System.out.println("\n");
			//enemyGroup,army ready
			
			//find no of soldiers in attack+1 radius. If more than enemy ants, attack
			Set<Tile> armyLevel1=new HashSet<Tile>();
			Set<Tile> armyLevel2=new HashSet<Tile>();
			Set<Tile> armyLevel3=new HashSet<Tile>();
			Set<Tile> armyLevel3Plus=new HashSet<Tile>();
			for(Tile soldier:army)
			{
				if(combatMatrix[soldier.getRow()][soldier.getCol()]==1)
					armyLevel1.add(soldier);
				else if(combatMatrix[soldier.getRow()][soldier.getCol()]==2)
					armyLevel2.add(soldier);
				else if(combatMatrix[soldier.getRow()][soldier.getCol()]==3)
					armyLevel3.add(soldier);
				else
					armyLevel3Plus.add(soldier);
			}
			//check if army1 can march by doing battle resolution
			
			//if(armyLevel1.size()>enemyGroup.size())
			Map<Tile,Tile> moves=new HashMap<Tile,Tile>();//destination,source
			Set<Tile> emptyTargets=new HashSet<Tile>();
			moves=funkyMoves(combatMatrix,armyLevel1,emptyTargets,0);
			
			Map<Tile,Tile> newMoves=null;
			if(battleResolutionAttackPlusOneOffsets(moves.keySet(),enemyGroup,ants.getEnemyAnts())<0)
			{
				//attack
				doMoveDirectionSet(moves);
				for(Tile soldierL1Moves:moves.keySet())
				{
					armyLevel1.remove(soldierL1Moves);
				}
				//L2->L1
				newMoves=funkyMoves(combatMatrix,armyLevel2,armyLevel1,1);
				doMoveDirectionSet(newMoves);
				for(Tile t:newMoves.keySet())
				{
					armyLevel2.remove(t);
				}
				//L3->L2
				newMoves=funkyMoves(combatMatrix,armyLevel3,armyLevel2,2);
				doMoveDirectionSet(newMoves);

				//L3+->L2
				for(Tile soldierL3:armyLevel3Plus)
				{
					for(Direction d:Direction.values())
					{
						Tile tryTile=ants.getTile(soldierL3, d);
						if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]<combatMatrix[soldierL3.getRow()][soldierL3.getCol()])
						{
							if(doMoveDirectionEnemyAware(soldierL3,d)==true)
								break;
						}
					}
				}
				
			}
			else 
			{
				newMoves=funkyMoves(combatMatrix,armyLevel2,armyLevel1,1);
				Set<Tile> army21Locs=new HashSet<Tile>();
				army21Locs.addAll(newMoves.keySet());
				army21Locs.addAll(armyLevel1);
				//if(armyLevel2.size()+armyLevel1.size()>enemyGroup.size())
				if(battleResolutionAttackPlusOneOffsets(army21Locs,enemyGroup,ants.getEnemyAnts())<0)
				{
					//L2->L1 and also L1 rearrange
					doMoveDirectionSet(newMoves);
					for(Tile t:newMoves.keySet())
					{
						armyLevel2.remove(t);
					}
					//L3->L2
					newMoves=funkyMoves(combatMatrix,armyLevel3,armyLevel2,2);
					doMoveDirectionSet(newMoves);
					//L3+->L2
					for(Tile soldierL3:armyLevel3Plus)
					{
						for(Direction d:Direction.values())
						{
							Tile tryTile=ants.getTile(soldierL3, d);
							if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]<combatMatrix[soldierL3.getRow()][soldierL3.getCol()])
							{
								if(doMoveDirectionEnemyAware(soldierL3,d)==true)
									break;
							}
						}
					}
				}
				else if(army.size()<=(enemyGroup.size()/2))//we wont get enough - run scenario
				{
					newMoves=funkyMoves(combatMatrix,armyLevel1,armyLevel2,2);
					doMoveDirectionSet(newMoves);
					for(Tile t:newMoves.keySet())
					{
						armyLevel1.remove(t);
					}
					Set<Tile> toRemove=new HashSet<Tile>();
					for(Tile s:armyLevel1)
					{
						for(Direction d:Direction.values())
						{
							Tile tryTile=ants.getTile(s, d);
							if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]==1)
							{
								if(doMoveDirectionEnemyAware(s,d)==true)
								{
									toRemove.add(s);
									break;
								}
							}
						}
					}
					
					for(Tile s:armyLevel2)
					{
						for(Direction d:Direction.values())
						{
							Tile tryTile=ants.getTile(s, d);
							if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]>combatMatrix[s.getRow()][s.getCol()])
							{
								if(doMoveDirectionEnemyAware(s,d)==true)
								{
									toRemove.add(s);
									break;
								}
							}
						}
					}
					armyLevel2.removeAll(toRemove);
					toRemove.clear();
					for(Tile s:armyLevel2)
					{
						for(Direction d:Direction.values())
						{
							Tile tryTile=ants.getTile(s, d);
							if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]==2)
							{
								if(doMoveDirectionEnemyAware(s,d)==true)
								{
									break;
								}
							}
						}
					}
				}
				else//no need to run now. lets wait for ppl
				{
					newMoves=funkyMoves(combatMatrix,armyLevel1,armyLevel2,2);
					doMoveDirectionSet(newMoves);
					
					/*Set<Tile> army21=new HashSet<Tile>();
					army21.addAll(newMoves.keySet());
					army21.addAll(armyLevel2);
					//L3->L2
					newMoves=funkyMoves(combatMatrix,armyLevel3,army21,2);
					doMoveDirectionSet(newMoves);*/
					for(Tile soldierL3:armyLevel3)
					{
						for(Direction d:Direction.values())
						{
							Tile tryTile=ants.getTile(soldierL3, d);
							if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]<combatMatrix[soldierL3.getRow()][soldierL3.getCol()])
							{
								if(doMoveDirectionEnemyAware(soldierL3,d)==true)
									break;
							}
						}
					}
					//L3+->L2
					for(Tile soldierL3:armyLevel3Plus)
					{
						for(Direction d:Direction.values())
						{
							Tile tryTile=ants.getTile(soldierL3, d);
							if(combatMatrix[tryTile.getRow()][tryTile.getCol()]!=-1 && combatMatrix[tryTile.getRow()][tryTile.getCol()]<combatMatrix[soldierL3.getRow()][soldierL3.getCol()])
							{
								if(doMoveDirectionEnemyAware(soldierL3,d)==true)
									break;
							}
						}
					}
				}
			}
		}
		
	}
	//returns a hashMap which will be the set of moves
	private HashMap<Tile,Tile> funkyMoves(int combatMatrix[][],Set<Tile> sourceLine,Set<Tile> targetLine,int targetLineValue)
	{
		HashMap<Tile,Tile> moves=new HashMap<Tile,Tile>();
		Set<Tile> notMoved=new HashSet<Tile>();
		//first move maximum possible - from source line to the target line
		boolean isMoved=false;
		for(Tile s1:sourceLine)
		{
			isMoved=false;
			for(Direction d:Direction.values())
			{
				Tile tryTile=ants.getTile(s1, d);
				if(!targetLine.contains(tryTile) && combatMatrix[tryTile.getRow()][tryTile.getCol()]==targetLineValue)
				{
					if(!moves.containsKey(tryTile))
					{
						isMoved=true;
						moves.put(tryTile,s1);
						break;
					}
					else
					{
						Tile os=moves.get(tryTile);
						for(Direction d2:Direction.values())
						{
							if(!d2.equals(tryTile))
							{
								Tile nd=ants.getTile(os, d2);
								if(!targetLine.contains(nd) && combatMatrix[nd.getRow()][nd.getCol()]==targetLineValue && !moves.containsKey(nd))
								{
									isMoved=true;
									moves.put(nd, os);
									moves.put(tryTile, s1);
								}
							}
						}
					}
				}
			}
			if(isMoved==false)
			{
				notMoved.add(s1);
			}
		}
		//second - for people who could not move to target, try adjusting
		outer:
		for(Tile s2:notMoved)
		{
			for(Direction d:Direction.values())
			{
				Tile nmd=ants.getTile(s2, d);
				if(targetLine.contains(nmd)){
					//try to move nmd to some free location
					for(Direction d2:Direction.values())
					{
						Tile tld=ants.getTile(nmd, d2);
						if(combatMatrix[tld.getRow()][tld.getCol()]==targetLineValue && !targetLine.contains(tld) && !moves.containsKey(tld))
						{
							moves.put(tld, nmd);
							moves.put(nmd,s2);
							continue outer;
						}
					}
				}
			}
		}
		return moves;
	}
	private int battleResolution(Set<Tile> army,Set<Tile> enemies)
	{
		HashMap<Tile,Integer> focusVals=new HashMap<Tile,Integer>();
		//calculate no of enemy ants in their attack radius
		for(Tile ant:army)
		{
			int count=0;
			for(Tile offset:ants.getAttackRadiusOffsets())
			{
				if(enemies.contains(ants.getTile(ant, offset)))
					count++;
			}
			focusVals.put(ant, count);
		}
		for(Tile ant:enemies)
		{
			int count=0;
			for(Tile offset:ants.getAttackRadiusOffsets())
			{
				if(army.contains(ants.getTile(ant, offset)))
					count++;
			}
			focusVals.put(ant, count);
		}
		//calculate the loss
		int armyloss=0;
		for(Tile ant:army)
		{
			int val=focusVals.get(ant);
			for(Tile offset:ants.getAttackRadiusOffsets())
			{
				Tile enemy=ants.getTile(ant, offset);
				if(enemies.contains(enemy))
				{
					if(focusVals.get(enemy)<=val)
						armyloss++;
				}
			}
		}
		int enemyloss=0;
		for(Tile ant:enemies)
		{
			int val=focusVals.get(ant);
			for(Tile offset:ants.getAttackRadiusOffsets())
			{
				Tile enemy=ants.getTile(ant, offset);
				if(army.contains(enemy))
				{
					if(focusVals.get(enemy)<=val)
						armyloss++;
				}
			}
		}
		return armyloss-enemyloss;
	}
	private int battleResolutionAttackPlusOneOffsets(Set<Tile> army,Set<Tile> enemies,Set<Tile> allEnemies)
	{
		HashMap<Tile,Integer> focusVals=new HashMap<Tile,Integer>();
		//calculate no of enemy ants in their attack radius
		Set<Tile> attackableEnemies=new HashSet<Tile>();
		for(Tile ant:army)
		{
			int count=0;
			for(Tile offset:ants.getAttackOffsets())
			{
				Tile c=ants.getTile(ant, offset);
				if(allEnemies.contains(c))
				{
					attackableEnemies.add(c);
					count++;
				}
			}
			focusVals.put(ant, count);
		}
		for(Tile ant:attackableEnemies)
		{
			int count=0;
			for(Tile offset:ants.getAttackOffsets())
			{
				if(army.contains(ants.getTile(ant, offset)))
					count++;
			}
			focusVals.put(ant, count);
		}
		//calculate the loss
		int armyloss=0;
		for(Tile ant:army)
		{
			int val=focusVals.get(ant);
			for(Tile offset:ants.getAttackOffsets())
			{
				Tile enemy=ants.getTile(ant, offset);
				if(attackableEnemies.contains(enemy))
				{
					if(focusVals.get(enemy)<=val)
						armyloss++;
				}
			}
		}
		int enemyloss=0;
		for(Tile ant:attackableEnemies)
		{
			int val=focusVals.get(ant);
			for(Tile offset:ants.getAttackOffsets())
			{
				Tile enemy=ants.getTile(ant, offset);
				if(army.contains(enemy))
				{
					if(focusVals.get(enemy)<=val)
						enemyloss++;
				}
			}
		}
		return armyloss-enemyloss;
	}
	private void combat(Set<Tile> availableAnts)
	{
		Set<Tile> enemyAnts=ants.getEnemyAnts();
		TreeSet<Tile> sortedEnemyAnts=new TreeSet<Tile>(enemyAnts);
		boolean exploredMap[][]=ants.getExploredMap();
		TileType map[][]=ants.getMap();
		
		int r=ants.getRows();int c=ants.getCols();
		int combatMatrix[][]=new int[r][c];

		
		//create enemy groups
		Set<Tile> enemyProcessed=new HashSet<Tile>();
		for(Tile enemy:sortedEnemyAnts)
		{
			for(int i=0;i<ants.getRows();i++)
				Arrays.fill(combatMatrix[i], -1);
			if(enemyProcessed.contains(enemy))
				continue;
			Set<Tile> enemyGroup=new HashSet<Tile>();
			enemyGroup.add(enemy);
			enemyProcessed.add(enemy);
			
			for(Tile offset:ants.getAttackOffsets())//this is attack offsets+1
			{
				Tile enemy2=ants.getTile(enemy, offset);
				if(enemyAnts.contains(enemy2))
				{
					enemyGroup.add(enemy2);
					enemyProcessed.add(enemy2);
				}
			}
			if(enemyGroup.size()<=1)
				continue;
			
			//Idea: For this group, create combat distances, select ants and do combat march/attack phase
			//For the next group remaining ants used. Groups has to be prioritized later
			//BFS here should consider water tiles also
			LinkedList<Tile> queue=new LinkedList<Tile>();
			int visited[][]=new int[r][c];
			for(Tile e:enemyGroup)
			{
				for(Tile offset:ants.getAttackRadiusOffsets())
				{
					Tile n=ants.getTile(e, offset);
					combatMatrix[n.getRow()][n.getCol()]=0;
					queue.add(n);
					visited[n.getRow()][n.getCol()]=1;
				}
			}
			Set<Tile> army=new HashSet<Tile>();
			
			while(queue.size()!=0)
			{
				Tile cur=queue.remove();
				int newr,newc;
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==1)
						continue;
					else
					{
						visited[newr][newc]=1;
						combatMatrix[newr][newc]=combatMatrix[cur.getRow()][cur.getCol()]+1;
						if(availableAnts.contains(newNode))
						{
							army.add(newNode);
							availableAnts.remove(newNode);
							if((2*army.size())>=enemyGroup.size())
								break;
						}
						if(combatMatrix[newr][newc]<6)//control of scent propagation
							queue.add(newNode);
					}
					
				}
			}

			//dispMatrix(combatMatrix);
			//System.out.println("\n");
			//enemyGroup,army ready
			
			//find no of soldiers in attack+1 radius. If more than enemy ants, attack
			Set<Tile> armyLevel1=new HashSet<Tile>();
			Set<Tile> armyLevel2=new HashSet<Tile>();
			Set<Tile> armyLevel3AndPlus=new HashSet<Tile>();
			for(Tile soldier:army)
			{
				if(combatMatrix[soldier.getRow()][soldier.getCol()]==1)
					armyLevel1.add(soldier);
				else if(combatMatrix[soldier.getRow()][soldier.getCol()]==2)
					armyLevel2.add(soldier);
				else
					armyLevel3AndPlus.add(soldier);
			}
			//we have outnumbered enemy
			if(armyLevel1.size()>enemyGroup.size())
			{
				//find no of soldiers who could march to attack radius, if this is less than or equal to
				//enemy count, then do not attack  - NOT IMPLEMENTED, affects when there is water
				for(Tile soldierL1:armyLevel1)
				{
					for(Direction d:Direction.values())
					{
						Tile tryTile=ants.getTile(soldierL1, d);
						if(combatMatrix[tryTile.getRow()][tryTile.getCol()]==0)
						{
							if(doMoveDirection(soldierL1,d)==true)
								break;
						}
					}
				}
				
			}
			else if(armyLevel2.size()>enemyGroup.size())
			{
				//find no of soldiers who could march to attack radius, if this is less than or equal to
				//enemy count, then do not attack  - NOT IMPLEMENTED, affects when there is water
				for(Tile soldierL2:armyLevel2)
				{
					for(Direction d:Direction.values())
					{
						Tile tryTile=ants.getTile(soldierL2, d);
						if(combatMatrix[tryTile.getRow()][tryTile.getCol()]==1)
						{
							if(doMoveDirection(soldierL2,d)==true)
								break;
						}
					}
				}
				
			}
			//start moving towards attack radius + 2
			else
			{
				for(Tile soldierL1:armyLevel1)
				{
					for(Direction d:Direction.values())
					{
						Tile tryTile=ants.getTile(soldierL1, d);
						if(combatMatrix[tryTile.getRow()][tryTile.getCol()]==2)
						{
							if(doMoveDirection(soldierL1,d)==true)
								break;
						}
					}
				}
				for(Tile soldierL3:armyLevel3AndPlus)
				{
					for(Direction d:Direction.values())
					{
						Tile tryTile=ants.getTile(soldierL3, d);
						if(combatMatrix[tryTile.getRow()][tryTile.getCol()]<combatMatrix[soldierL3.getRow()][soldierL3.getCol()])
						{
							if(doMoveDirection(soldierL3,d)==true)
								break;
						}
					}
				}
				
			}
		}
		
	}
	private void findSuckingZones(boolean suckingCells[][],int exploreMatrix[][])
	{
		int r=ants.getRows();int c=ants.getCols();
		TileType map[][]=ants.getMap();
		boolean [][]exploredMap=ants.getExploredMap();
		//find sucking roots
		Set<Tile> suckingRoots=new HashSet<Tile>();
		for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				Tile cur=new Tile(i,j);
				boolean isSuckingRoot=true;
				for(Direction d:Direction.values())
				{
					Tile next=ants.getTile(cur, d);
					if(exploreMatrix[i][j]<=exploreMatrix[next.getRow()][next.getCol()] || exploredMap[next.getRow()][next.getCol()]==false)
					{
						isSuckingRoot=false;
						break;
					}
				}
				if(isSuckingRoot==true)
				{
					int watercount=0;
					for(Direction d:Direction.values())
					{
						Tile next=ants.getTile(cur, d);
						//if(exploreMatrix[next.getRow()][next.getCol()]==-1)
						if(map[next.getRow()][next.getCol()].equals(TileType.WATER))
						{
							watercount++;
						}
					}
					if(turn<=100)
					{
						suckingRoots.add(new Tile(i,j));
						suckingCells[i][j]=true;
					}
					else if(turn>100 && watercount>=1)
					{
						suckingRoots.add(new Tile(i,j));
						suckingCells[i][j]=true;
					}
					
				}
			}	
		}
		//find all sucking cells now
		for(Tile t:suckingRoots)
		{
			int visited[][]=new int[r][c];

			LinkedList<Tile> q=new LinkedList<Tile>();
			q.add(t);
			visited[t.getRow()][t.getCol()]=1;
			
			while(q.size()!=0)//it may not be able to reach the target itself
			{
				Tile cur=q.remove();
				int newr,newc;
				
				
					for(Direction d:Direction.values())
					{
						Tile newNode=ants.getTile(cur, d);
						newr=newNode.getRow();newc=newNode.getCol();
						if(visited[newr][newc]==1)
							continue;
						if(exploreMatrix[newr][newc]==-1)
							continue;
						//check whether the newnode is also a sucking cell, if so add it to the queue
						boolean suckingCellFound=true;
						for(Direction d2:Direction.values())
						{
							Tile x=ants.getTile(newNode,d2);
							if(exploreMatrix[x.getRow()][x.getCol()]>exploreMatrix[newr][newc] && suckingCells[x.getRow()][x.getCol()]==false)
								suckingCellFound=false;//there is some way out
						}
						if(suckingCellFound==true)
						{
							visited[newr][newc]=1;
							q.add(newNode);
							suckingCells[newr][newc]=true;
						}
					}
				
			}

		}
		
	}
	private void updateExploreMatrix(int [][]stepMatrix,boolean add)
	{
		for(int i=0;i<stepMatrix.length;i++)
		{
			for(int j=0;j<stepMatrix[0].length;j++)
			{
				if(add)
					exploreMatrix[i][j]=exploreMatrix[i][j]+stepMatrix[i][j];
				else
				exploreMatrix[i][j]=exploreMatrix[i][j]-stepMatrix[i][j];	
			}
		}
	}
	int x[]={0,-1,1,-2,2,-1,1,0};
	int y[]={2,1,1,0,0,-1,-1,-2};
	private Set<Tile> findTraps(boolean exploredMap[][],int exploreMatrix[][])
	{
		int rows=ants.getRows();
		int cols=ants.getCols();
		Set<Tile> traps=new HashSet<Tile>();
		for(int i=0;i<exploredMap.length;i++)
		{
			for(int j=0;j<exploredMap[0].length;j++)
			{
				boolean isTrap=true;
				if(exploredMap[i][j]==true)
				{
					int cur=exploreMatrix[i][j];
					for(int k=0;k<8;k++)
					{
						
						int newi=(i+x[k])%rows;int newj=(j+x[k])%cols;
						if(newi<0) newi=newi+rows;
						if(newj<0) newj=newj+cols;
						//if((exploreMatrix[newi][newj]-stepMatrix[newi][newj])>cur)
						if((exploreMatrix[newi][newj])>cur)
						{
							isTrap=false;
							break;
						}
					}
					if(isTrap==true)
						traps.add(new Tile(i,j));
				}
				
			}
		}
		return traps;
	}
	private void exploreOutwards(Set<Tile> availableAnts,int exploreMatrix[][],boolean [][]suckingCells,int maxVal)
	{
		Set<Tile> sortedAnts=new TreeSet<Tile>(availableAnts);
		int pick=0;
		for(Tile ant:sortedAnts)
		{
			pick++;
			boolean isSuckingCell=false;
			if(suckingCells[ant.getRow()][ant.getCol()]==true)
				isSuckingCell=true;
				//continue;
			int cur=exploreMatrix[ant.getRow()][ant.getCol()];
			Set<Direction> possibleMoves=new HashSet<Direction>();
			//if(turn<100)
				for(int i=0;i<shuffledDirs.size();i++)
				{
					Direction d;
					if(pick%2==0) 
						d=shuffledDirs.get(i);
					else
						d=shuffledDirs2.get(i);
					Tile newTile=ants.getTile(ant, d);
					if(exploreMatrix[newTile.getRow()][newTile.getCol()]==maxVal && suckingCells[newTile.getRow()][newTile.getCol()]==false)
						possibleMoves.add(d);
					if(isSuckingCell==true && exploreMatrix[newTile.getRow()][newTile.getCol()]<cur)
						possibleMoves.add(d);
					else if(isSuckingCell==false && exploreMatrix[newTile.getRow()][newTile.getCol()]>cur && suckingCells[newTile.getRow()][newTile.getCol()]==false)
						possibleMoves.add(d);
					/*else if(isSuckingCell==false && exploreMatrix[newTile.getRow()][newTile.getCol()]==maxVal && suckingCells[newTile.getRow()][newTile.getCol()]==false)
						possibleMoves.add(d);*/
					/*if(exploreMatrix[newTile.getRow()][newTile.getCol()]>cur && suckingCells[newTile.getRow()][newTile.getCol()]==false)
					possibleMoves.add(d);*/
				}
			/*else
			{
				possibleMoves.add(Direction.NORTH);possibleMoves.add(Direction.SOUTH);
				possibleMoves.add(Direction.EAST);possibleMoves.add(Direction.WEST);
			}*/
			int min=Integer.MAX_VALUE;
			Direction maxDir=null;
			int possibleWays=possibleMoves.size();
			Set<Direction> allMoves=new HashSet<Direction>(shuffledDirs);
			boolean isAlreadyMoved=false;
			for(int k=0;k<possibleWays;k++)
			{
				for(Direction d:possibleMoves)
				{
					Tile m=ants.getTile(ant, d);
					if(stepMatrix[m.getRow()][m.getCol()]<min)
					{
						min=stepMatrix[m.getRow()][m.getCol()];
						maxDir=d;
					}
				}
				//if(maxDir!=null)
				
				possibleMoves.remove(maxDir);
				allMoves.remove(maxDir);
			
				if(doMoveDirectionEnemyAware(ant,maxDir)==true)
				{
					isAlreadyMoved=true;
					break;
				}
				min=Integer.MAX_VALUE;maxDir=null;
			}
			if(isAlreadyMoved==false)
			{
				for(Direction d:allMoves)
				{
					if(doMoveDirectionEnemyAware(ant,d)==true)
					{
						break;
					}
				}
			}
		}
		
	}
	private void reachPotentialRegions(Set<Tile> availableAnts)
	{
		Set<Tile> sortedPotentialTiles=new TreeSet<Tile>();
		for(Tile region:potentialRegions)
		{
			sortedPotentialTiles.add(new Tile(region.getRow()*lengthA,region.getCol()*lengthA));
		}
		gatherFood_ShortestDistanceFirst(availableAnts,sortedPotentialTiles,false);/*
		for(Tile food:sortedPotentialTiles)
		{
			if(availableAnts.size()>0)
			{
				Order order=PathFind.bfsDirectionSet(ants.getMap(), food, availableAnts,ants);
				if(order!=null)
				{
					availableAnts.remove(order.getTile());
					doMoveDirection(order.getTile(),order.getDirection());
				}
			}
			//break;
		}*/
	}
	Set<Tile> potentialRegions=new HashSet<Tile>();
	int rrows,rcols,lengthA;
	int regions[][];
	boolean firsttime=true;
	private void createRegions()//this can get dynamic also
	{
		if(firsttime)
		{
			lengthA=(int)Math.sqrt(ants.getViewRadius2());
			lengthA=(lengthA/2);
			int rows=ants.getRows();int cols=ants.getCols();
			rrows=(ants.getRows()%lengthA==0)?ants.getRows()/lengthA:1+(ants.getRows()/lengthA);
			rcols=(ants.getCols()%lengthA==0)?ants.getCols()/lengthA:1+(ants.getCols()/lengthA);
			regions=new int[rrows][rcols];
			for(int i=0;i<rrows;i++)
				for(int j=0;j<rcols;j++)
					regions[i][j]=-1;
			findWaterRegions(regions,50);
		}
		else
		{
			for(int i=0;i<rrows;i++)
				for(int j=0;j<rcols;j++)
					regions[i][j]=-1;
			findWaterRegions(regions,50);
		}
	}
	private void getPotentialRegions(Set<Tile> availableAnts)
	{
		//updateExploredRegions
		createRegions();
		boolean exploredMap[][]=ants.getExploredMap();
		int rows=ants.getRows();int cols=ants.getCols();
		for(int rowi=0;rowi<rows;rowi++)
			for(int colj=0;colj<cols;colj++)
				if(exploredMap[rowi][colj]==true)
					regions[rowi/lengthA][colj/lengthA]=1;
		//updateFoodRegions
		for(Tile food:ants.getFoodTiles())
		{
			regions[food.getRow()/lengthA][food.getCol()/lengthA]=2;
		}
		//check if ants have reached target regions, if so remove it
		/*for(Tile ant:ants.getMyAnts())
		{
			Tile antRegion=getRegion(ant);
			potentialRegions.remove(antRegion);
		}*/
		potentialRegions.clear();
		//update remaining target regions as already would be marching towards it
		for(Tile afood:potentialRegions)
		{
			regions[afood.getRow()/lengthA][afood.getCol()/lengthA]=3;
		}
		//findPotentialRegions, we have more ants, create more target regions for them
		if(availableAnts.size()>potentialRegions.size())
		{
			//exploration is less than 50%, explore only
			int x=findCountOfRegionsExplored();

			int required=availableAnts.size()-potentialRegions.size();
			int noOfHills=ants.getMyInitHills().size();
			int regionsRequiredPerHill=required/noOfHills;
			int extraRegions=required%noOfHills;
			for(Tile hill:ants.getMyInitHills())
			{
				int sx=hill.getRow();
				int sy=hill.getCol();
				int rx=sx/lengthA;
				int ry=sy/lengthA;
				if(extraRegions--!=0)
					potentialRegions.addAll((bfsUnseenRegion(rx,ry,regionsRequiredPerHill+1)));
				else
					potentialRegions.addAll((bfsUnseenRegion(rx,ry,regionsRequiredPerHill)));
			}
			//exploration is more, distribute some - Later
		}
	}
	static ArrayList<Direction> shuffledDirs=new ArrayList<Direction>();
	static ArrayList<Direction> shuffledDirs2=new ArrayList<Direction>();
	int exploreMatrix[][];
	int enemyHillMatrix[][];
	private void initExploreMatrix(int def)
	{
		if(stepMatrix==null)
		{
			stepMatrix=new int[ants.getRows()][ants.getCols()];
		}
		if(exploreMatrix==null)
		{
			exploreMatrix=new int[ants.getRows()][ants.getCols()];
			for(int i=0;i<ants.getRows();i++)
				Arrays.fill(exploreMatrix[i], def);
		}
		if(enemyHillMatrix==null)
		{
			enemyHillMatrix=new int[ants.getRows()][ants.getCols()];
			for(int i=0;i<ants.getRows();i++)
				Arrays.fill(enemyHillMatrix[i], def);
		}
	}

	static
	{
		shuffledDirs.add(Direction.NORTH);
		shuffledDirs.add(Direction.SOUTH);
		shuffledDirs.add(Direction.EAST);
		shuffledDirs.add(Direction.WEST);
		shuffledDirs2.add(Direction.WEST);
		shuffledDirs2.add(Direction.EAST);
		shuffledDirs2.add(Direction.NORTH);
		shuffledDirs2.add(Direction.SOUTH);
	}
	
	private Set<Tile> bfsUnseenRegion(int rx,int ry,int requiredNoOfRegions)
	{
		int countFound=0;
		Set<Tile> unexploredRegions=new HashSet<Tile>();
		Set<Tile> visited=new HashSet<Tile>();
		LinkedList<Tile> queue=new LinkedList<Tile>();
		queue.add(new Tile(rx,ry));
		visited.add(new Tile(rx,ry));
		while(queue.size()>0)
		{
			Tile cur=queue.remove();
			//will not bfs to water region
			if(regions[cur.getRow()][cur.getCol()]==-1 && regions[cur.getRow()][cur.getCol()]!=10)
			{
				countFound++;
				unexploredRegions.add(cur);
				if(countFound==requiredNoOfRegions)
				break;
			}
			else
			{
				
				//for(Direction d:Direction.values())
				for(Direction d:shuffledDirs)
				{
					Tile newNode=getRegionsTile(cur, d);
	
					if(!visited.contains(newNode))
					{
						visited.add(newNode);
						queue.add(newNode);
						
					}
				}
			}
		}
		return unexploredRegions;
	}
	public Tile getRegionsTile(Tile tile, Direction direction) {
        int row = (tile.getRow() + direction.getRowDelta()) % rrows;
        if (row < 0) {
            row += rrows;
        }
        int col = (tile.getCol() + direction.getColDelta()) % rcols;
        if (col < 0) {
            col += rcols;
        }
        return new Tile(row, col);
    }
	private int findCountOfRegionsExplored()
	{
		int count=0;
		for(int i=0;i<rrows;i++)
			for(int j=0;j<rcols;j++)
				if(regions[i][j]==0)
					count++;
		return count;
	}
	private void preventHillStepping()
	{
		for (Tile myHill : ants.getMyHills()) {
			orders.put(myHill, null);
		}
	}
	private void gatherFood(Set<Tile> availableAnts)
	{
		TreeSet<Tile> sortedFood=new TreeSet<Tile>(ants.getFoodTiles());//imp
		for(Tile food:sortedFood)
		{
			if(availableAnts.size()>0)
			{
				//order will have <ant selected for the food,direction in which the ant should move>
				Order order=PathFind.bfsDirectionSet(ants.getMap(), food, availableAnts,ants);
				if(order!=null)
				{
					availableAnts.remove(order.getTile());
					doMoveDirection(order.getTile(),order.getDirection());
				}
			}
		}
	}
	private void gatherFood_AntsToFood(Set<Tile> availableAnts)
	{
		Set<Tile> food=ants.getFoodTiles();
		Set<Tile> availableFoods=new HashSet<Tile>();
		for(Tile f:food)
			availableFoods.add(new Tile(f.getRow(),f.getCol()));
		//Set<Tile> antsToBeRemoved=new HashSet<Tile>();
		TreeSet<Tile> sortedAnts=new TreeSet<Tile>(availableAnts);
		for(Tile ant:sortedAnts)
		{
				//order will have <food selected for the ant,direction in which ant should move>
				Order order=PathFind.bfsAntsToFood(ants.getMap(), ant, availableFoods, ants);
				if(order!=null)
				{
					//antsToBeRemoved.add(ant);
					availableAnts.remove(ant);
					availableFoods.remove(order.getTile());
					doMoveDirection(ant,order.getDirection());
				}
			
		}
		//availableAnts.removeAll(antsToBeRemoved);
	}
	
	private void gatherFood_ShortestDistanceFirst(Set<Tile> availableAnts,Set<Tile> foodTiles,boolean club)
	{
		//creates a distance matrix from every ant to every other food and orders it by the distance
		ArrayList<Tile> orderedFoods=new ArrayList<Tile>(foodTiles);
		ArrayList<Tile> orderedAnts=new ArrayList<Tile>(availableAnts);//this is the ordering of the ants we will working in this method
		int distanceMatrix[][]=new int[orderedFoods.size()][orderedAnts.size()];
		Direction directionMatrix[][]=new Direction[orderedFoods.size()][orderedAnts.size()];
		int maxTurns=3000;//this itself is a very high no. tweak it if it times out
		for(int i=0;i<orderedFoods.size();i++)
		{
			PathFind.bfsFoodToAllAntsMatrix(ants.getMap(),orderedFoods.get(i),orderedAnts,distanceMatrix,directionMatrix,i,maxTurns);
		}
		//we have the complete matrix, each time select the shortest and eliminate that food and ant for the next iterations
		//remove the ants from the available ants and order the ant to move towards the selected food.
		ArrayList<Integer> findex=new ArrayList<Integer>();
		ArrayList<Integer> aindex=new ArrayList<Integer>();
		ArrayList<Tile> antsToRemove=new ArrayList<Tile>();
		for(int i=0;i<orderedFoods.size();i++)
			findex.add(i);
		for(int i=0;i<orderedAnts.size();i++)
			aindex.add(i);
		for(int k=0;k<orderedFoods.size();k++)
		{
			int minDist=Integer.MAX_VALUE;
			int locI=Integer.MAX_VALUE,locJ=Integer.MAX_VALUE;
			for(Integer i:findex)
			{
				for(Integer j:aindex)
				{
					if(distanceMatrix[i][j]<minDist && distanceMatrix[i][j]>0)
					{
						minDist=distanceMatrix[i][j];
						locI=i;locJ=j;
					}
				}
			}
			//at this point we have found a pairing, remove them as they cant be considered again
			if(minDist==Integer.MAX_VALUE)
				break;
			if(club)
			{
				Set<Tile> clubbedFoods=clubFoods(orderedFoods.get(locI));
				for(Tile food:clubbedFoods)
				{
					findex.remove(new Integer(orderedFoods.indexOf(food)));
				}
			}
			findex.remove(new Integer(locI));
			aindex.remove(new Integer(locJ));
			//availableAnts.remove(orderedAnts.get(locJ));
			antsToRemove.add(orderedAnts.get(locJ));
			doMoveDirectionEnemyAware(orderedAnts.get(locJ),directionMatrix[locI][locJ]);
			if(aindex.size()==0 || findex.size()==0)
				break;
		}
		availableAnts.removeAll(antsToRemove);
	}
	private static final Set<Tile> foodOffsets;
	static
	{
	foodOffsets = new HashSet<Tile>();
    int mx = (int)Math.sqrt(9);
    for (int row = -mx; row <= mx; ++row) {
        for (int col = -mx; col <= mx; ++col) {
            int d = row * row + col * col;
            if (d <= 9) {
                foodOffsets.add(new Tile(row, col));
            }
        }
    }
	}
	private Set<Tile> clubFoods(Tile source)
	{
			Set<Tile> clubbedFoods=new HashSet<Tile>();
            for (Tile locOffset : foodOffsets) {
                Tile newLoc = ants.getTile(source, locOffset);
                clubbedFoods.add(newLoc);
            }
            return clubbedFoods;
	}
	private void unblockHills()
	{
		for (Tile myHill : ants.getMyHills()) {
			if (ants.getMyAnts().contains(myHill) && !orders.containsValue(myHill)) {
				for (Direction direction : Direction.values()) {
					if (doMoveDirection(myHill, direction)) {
						break;
					}       
				}
			}
		}
	}
	private void explore_BFS_unseen(Set<Tile> availableAnts)
	{
		boolean [][]exploredMap=ants.getExploredMap();
		/*boolean [][]exploredMapWithVision=new boolean[ants.getRows()][ants.getCols()];
		for(int i=0;i<ants.getRows();i++)
			for(int j=0;j<ants.getCols();j++)
				exploredMapWithVision[i][j]=exploredMap[i][j];*/
		for(Tile ant:availableAnts)
		{

			Direction d=PathFind.bfsAntsToUnseenNodes(exploredMap,ant,ants.getRows()*ants.getCols()/2);
			if(d!=null)
			{
				doMoveDirection(ant,d);
			}
		}
	}
	private void findWaterRegions(int regions[ ][],int cutoff)
	{
		TileType [][]map=ants.getMap();
		//List<Tile> waterRegions=new ArrayList<Tile>();
		int noOfCellsInRegion=lengthA*lengthA;
		for(int i=0;i<rrows;i++)
		{
			for(int j=0;j<rcols;j++)
			{
				//For each region, check how many water squares are there and add it to a list if its more than threshold
				int count=0;
				for(int ri=i*lengthA;ri<(i*lengthA)+lengthA;ri++)
					for(int rj=i*lengthA;rj<(i*lengthA)+lengthA;rj++)
					{
						if(ri<ants.getRows() && rj<ants.getCols())
						{
							if(map[ri][rj]==TileType.WATER)
								count++;
						}
					}
				if(((count/noOfCellsInRegion)*100)>cutoff)
					regions[i][j]=10;
			}
		}
		//return waterRegions;
	}
	
	private Tile getRegion(Tile t)
	{
		
		return new Tile(t.getRow()/lengthA,t.getCol()/lengthA);
	}
	private void dispMatrix(int [][]matrix)
	{
		for(int i=0;i<matrix.length;i++)
		{
			for(int j=0;j<matrix[0].length;j++)
				System.out.print(matrix[i][j]+"  ");
			System.out.println();
		}
	}
	private void dispBoolMatrix(boolean [][]matrix,TileType map[][])
	{
		for(int i=0;i<matrix.length;i++)
		{
			for(int j=0;j<matrix[0].length;j++)
			{
				if(map[i][j]==TileType.WATER)
					System.out.print(2+"  ");	
				else if(matrix[i][j]==true)
				System.out.print(1+"  ");
				else
					System.out.print(0+"  ");
			}
			
			System.out.println();
		}
	}
	private void createWaterHeatMapMatrix(int [][]waterHeatMapMatrix)
	{
		boolean exploreMap[][]=ants.getExploredMap();
		TileType map[][]=ants.getMap();
		int visited[][]=new int[map.length][map[0].length];

		LinkedList<Tile> q=new LinkedList<Tile>();
		for(int i=0;i<map.length;i++)
			for(int j=0;j<map[0].length;j++)
			{
				if(map[i][j].equals(TileType.WATER))
				{
				q.add(new Tile(i,j));
				visited[i][j]=1;
				waterHeatMapMatrix[i][j]=2000;
				}
			}
		
		while(q.size()!=0)//it may not be able to reach the target itself
		{
			Tile cur=q.remove();
			int newr,newc;
			
			
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER && exploreMap[newr][newc]==true)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						waterHeatMapMatrix[newr][newc]=waterHeatMapMatrix[cur.getRow()][cur.getCol()]+2500;
					}
				}
			
		}
		
	}
}
