import java.util.*;
public class PathFind {
	static Ants ants;
	
	public static Order bfsDirectionSet(TileType map[][],Tile source,Set<Tile> target,Ants ants)
	{
		if(target.contains(source))
			return null;
		boolean targetFound=false;
		int visited[][]=new int[map.length][map[0].length];
		Tile parent_map[][]=new Tile[map.length][map[0].length];
		LinkedList<Tile> q=new LinkedList<Tile>();
		Tile cur=source;
		
		q.add(source);
		parent_map[source.getRow()][source.getCol()]=null;
		visited[cur.getRow()][cur.getCol()]=1;
		while(q.size()!=0)//it may not be able to reach the target itself
		{
			cur=q.remove();
			int newr,newc;
			
			if(target.contains(cur))
			{
				targetFound=true;
				break;
			}
			else
			{
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						parent_map[newr][newc]=cur;
					}
				}
			}
		}
		if(targetFound)
		{
			//cur will be the ant(ant selected)
			//cur's parent - thats the direction to move
			return new Order(cur,Utils.whichDirection(cur, parent_map[cur.getRow()][cur.getCol()],ants));
		}
		else
		{
			return null;
		}
		
	}
	public static Order bfsAntsToFood(TileType map[][],Tile source,Set<Tile> target,Ants ants)
	{
		if(target.contains(source))
			return null;
		boolean targetFound=false;
		int visited[][]=new int[map.length][map[0].length];
		Tile parent_map[][]=new Tile[map.length][map[0].length];
		LinkedList<Tile> q=new LinkedList<Tile>();
		Tile cur=source;
		
		q.add(source);
		parent_map[source.getRow()][source.getCol()]=null;
		visited[cur.getRow()][cur.getCol()]=1;
		while(q.size()!=0)//it may not be able to reach the target itself
		{
			cur=q.remove();
			int newr,newc;
			
			if(target.contains(cur))
			{
				targetFound=true;
				break;
			}
			else
			{
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						parent_map[newr][newc]=cur;
					}
				}
			}
		}
		Tile tempcur=cur;
		if(targetFound)
		{
			//cur will be the ant(ant selected)
			//cur's parent - thats the direction to move
			Tile prevCur=null;
			while(parent_map[cur.getRow()][cur.getCol()]!=null)
			{
				prevCur=cur;
				cur=parent_map[cur.getRow()][cur.getCol()];
			}
			return new Order(tempcur,Utils.whichDirection(cur, prevCur,ants));
		}
		else
		{
			return null;
		}
		
	}
	public static Direction bfsAntsToUnseenNodes(boolean exploredMap[][],Tile source,int maxTurns)
	{
		//get Required Info & initialize
		//boolean exploredMap[][]=ants.getExploredMap();
		TileType [][]map=ants.getMap();
		boolean targetFound=false;
		int visited[][]=new int[ants.getRows()][ants.getCols()];
		Tile parent_map[][]=new Tile[ants.getRows()][ants.getCols()];
		LinkedList<Tile> q=new LinkedList<Tile>();
		Tile cur=source;
		
		q.add(source);
		parent_map[source.getRow()][source.getCol()]=null;
		visited[cur.getRow()][cur.getCol()]=1;
		int j=0;
		while(q.size()!=0 && j++<maxTurns)//it may not be able to reach the target itself
		{
			cur=q.remove();
			int newr,newc;
			
			if(exploredMap[cur.getRow()][cur.getCol()]==false)
			{
				targetFound=true;
				break;
			}
			else
			{
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						parent_map[newr][newc]=cur;
					}
				}
			}
		}
		if(targetFound)
		{
			//cur will be the unseen node
			//backtrack till you reach the ant
			Tile prevCur=null;
			while(parent_map[cur.getRow()][cur.getCol()]!=null)
			{
				prevCur=cur;
				cur=parent_map[cur.getRow()][cur.getCol()];
			}
			return Utils.whichDirection(cur, prevCur,ants);
		}
		else
		{
			return null;
		}
	
	}
	//PathFind.bfsFoodToAllAntsMatrix(map,orderedFoods.get(i),orderedAnts,distanceMatrix,directionMatrix,i,maxTurns);
	public static void bfsFoodToAllAntsMatrix(TileType map[][],Tile source,List<Tile> orderedAnts,int distanceMatrix[][],Direction directionMatrix[][],int findex,int maxTurns)
	{
		int visited[][]=new int[map.length][map[0].length];
		int dist[][]=new int[map.length][map[0].length];
		Tile parent_map[][]=new Tile[map.length][map[0].length];
		LinkedList<Tile> q=new LinkedList<Tile>();
		Tile cur=source;
		
		q.add(source);
		parent_map[source.getRow()][source.getCol()]=null;
		visited[cur.getRow()][cur.getCol()]=1;
		
		int turn=0;
		while(q.size()!=0 && turn++<maxTurns)//it may not be able to reach the target itself
		{
			cur=q.remove();
			int newr,newc;
			
			if(orderedAnts.contains(cur))
			{
				//new logic here,everytime we find one ant, update the distanceMatrix and directionMatrix
				//we have findex;
				int antindex=orderedAnts.indexOf(cur);
				distanceMatrix[findex][antindex]=dist[cur.getRow()][cur.getCol()];
				directionMatrix[findex][antindex]=Utils.whichDirection(cur, parent_map[cur.getRow()][cur.getCol()],ants);
			}
			else
			{
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						parent_map[newr][newc]=cur;
						dist[newr][newc]=dist[cur.getRow()][cur.getCol()]+1;
					}
				}
			}
		}
				
	}
	
	public static void bfsDistancesFromHill(TileType map[][],Set<Tile> myHills,int exploreMatrix[][],boolean exploreMap[][])
	{
		int visited[][]=new int[map.length][map[0].length];

		LinkedList<Tile> q=new LinkedList<Tile>();
		for(Tile cur:myHills)
		{
			q.add(cur);
			visited[cur.getRow()][cur.getCol()]=1;
			exploreMatrix[cur.getRow()][cur.getCol()]=1;
		}
		
		while(q.size()!=0)//it may not be able to reach the target itself
		{
			Tile cur=q.remove();
			int newr,newc;
			
			
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER && exploreMap[newr][newc]==true)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						exploreMatrix[newr][newc]=exploreMatrix[cur.getRow()][cur.getCol()]+1;
					}
				}
			
		}
		
	}
	public static void bfsDistancesFromEnemyHill(TileType map[][],Set<Tile> enemyHills,int exploreMatrix[][],boolean exploreMap[][])
	{
		int visited[][]=new int[map.length][map[0].length];

		LinkedList<Tile> q=new LinkedList<Tile>();
		for(Tile cur:enemyHills)
		{
			q.add(cur);
			visited[cur.getRow()][cur.getCol()]=1;
			exploreMatrix[cur.getRow()][cur.getCol()]=1;
		}
		
		while(q.size()!=0)//it may not be able to reach the target itself
		{
			Tile cur=q.remove();
			int newr,newc;
			
			
				for(Direction d:Direction.values())
				{
					Tile newNode=ants.getTile(cur, d);
					newr=newNode.getRow();newc=newNode.getCol();
					if(visited[newr][newc]==0 && map[newr][newc]!=TileType.WATER && exploreMap[newr][newc]==true)//not visited and isPassable
					{
						visited[newr][newc]=1;
						q.add(newNode);
						exploreMatrix[newr][newc]=exploreMatrix[cur.getRow()][cur.getCol()]+1;
					}
				}
			
		}
		
	}
}
