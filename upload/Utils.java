import java.util.ArrayList;
import java.io.*;
import java.util.Collections;


public class Utils
{
	public static ArrayList<Direction> getRandomDirs()
    {
    	ArrayList<Integer> shuffle_dirs=new ArrayList<Integer>();
    	shuffle_dirs.add(0);shuffle_dirs.add(1);shuffle_dirs.add(2);shuffle_dirs.add(3);
    	Collections.shuffle(shuffle_dirs);
    	ArrayList<Direction> Directions=new ArrayList<Direction>();
    	for(Integer i:shuffle_dirs){
    		Directions.add(Direction.values()[i]);
    	}
    	return Directions;
    }
	static StringBuffer sb=new StringBuffer();
	public static void appendLog(String data)
	{
		sb.append(data+"\n");
	}
	static BufferedWriter br;
	public static void log() throws IOException
	{
		
		if(br==null)
			br=new BufferedWriter(new FileWriter("C:\\Users\\pseluka\\Desktop\\out.txt"));
		br.write(sb.toString());
		br.flush();
		br.close();
		
	}
	public static Direction whichDirection(Tile from,Tile to,Ants ants)
	{
		for(Direction d:Direction.values())
		{
			Tile destination=ants.getTile(from, d);
			if(destination.equals(to))
				return d;
		}
		return null;
	/*	if(to.getRow()==from.getRow())
		{
			if(from.getCol()<to.getCol())
				return Direction.EAST;
			else
				return Direction.WEST;
		}
		else
		{
			if(to.getRow()>from.getRow())
				return Direction.SOUTH;
			else
				return Direction.NORTH;
		}*/
	}
}
