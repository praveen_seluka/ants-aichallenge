import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

class ParserBot
{
	final private int BUFFER_SIZE = 1 << 16;
	private DataInputStream din;
	private byte[] buffer;
	private int bufferPointer, bytesRead;
	public ParserBot(InputStream in)
	{
		din = new DataInputStream(in);
		buffer = new byte[BUFFER_SIZE];
		bufferPointer = bytesRead = 0;
	}
	public String nextString() throws IOException
	{
		StringBuffer sb=new StringBuffer("");
		byte c = read();
		while (c <= ' ') c = read();
		do
		{
			sb.append((char)c);
			c=read();
		}while(c!='\r');
		return sb.toString();
	}
 
	public int nextInt() throws IOException
	{
		int ret = 0;
		byte c = read();
		while (c <= ' ') c = read();
		boolean neg = c == '-';
		if (neg) c = read();
		do
		{
			ret = ret * 10 + c - '0';
			c = read();
		} while (c > ' ');
		if (neg) return -ret;
		return ret;
	}

	private void fillBuffer() throws IOException
	{
		bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
		if (bytesRead == -1) buffer[0] = -1;
	}
 
	private byte read() throws IOException
	{
		if (bufferPointer == bytesRead) fillBuffer();
		return buffer[bufferPointer++];
	}
}