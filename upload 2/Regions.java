public class Regions
{
	Ants ants;
	private int rows;
	private int cols;
	private int lengthA;
	private int regions[][];
	public Regions(int r,int c,int viewRadiusSquared,Ants ants)
	{
		lengthA=(int)Math.sqrt(ants.getViewRadius2());
    	lengthA=lengthA/2;
    	//create regions data structure
    	rows=(ants.getRows()%lengthA==0)?ants.getRows()/lengthA:1+(ants.getRows()/lengthA);
    	cols=(ants.getCols()%lengthA==0)?ants.getCols()/lengthA:1+(ants.getCols()/lengthA);
    	regions=new int[rows][cols];
    	for(int i=0;i<rows;i++)
    		for(int j=0;j<cols;j++)
    			regions[i][j]=-1;
	}
//	public int getRegionRow(int row)
//	{
//		
//	}
//	public int getRegionCol(int col)
//	{
//		
//	}
	public int getRows()
	{
		return rows;
	}
	public int getCols()
	{
		return cols;
	}
	public int getLengthA()
	{
		return lengthA;
	}
	public int[][] getRegions()
	{
		return regions;
	}
}